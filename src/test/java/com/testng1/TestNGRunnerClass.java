package com.testng1;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features="src/test/resources/features", glue="com.testng1")
public class TestNGRunnerClass extends AbstractTestNGCucumberTests{

}
